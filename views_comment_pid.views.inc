<?php

/**
 * Implements hook_views_data().
 */
function views_comment_pid_views_data_alter(&$data) {
  // Add a filter handler for the parent comment id.
  $data['comment']['pid']['filter'] = array(
    'handler' => 'views_handler_filter_numeric',
  );
  // Add the parent comment ID as argument
  $data['comment']['pid']['argument'] = array(
    'handler' => 'views_handler_argument_numeric',
  );
  // Add the parent comment ID as sort
  $data['comment']['pid']['sort'] = array(
    'handler' => 'views_handler_sort',
  );

  // Add a filter handler for the depth (if threaded).
  $data['comment']['pid']['thread'] = array(
    'handler' => 'views_handler_filter_numeric',
  );
}